open Ast;;
open Env;;
open Store;;

let memory = ref 0;;
type stmtEvalRes = Next | BreakOut | ContinueOn;;

let checkEnv (env:environment) = 
  List.iter
  (
    fun symTable ->
      List.iter
      (
        fun symEntry ->
          match symEntry with
          | (name, (Var entry)) ->
              print_endline(string_of_int entry.loc ^ ". " ^ name)              
          | (name, (Fun entry)) ->
              print_endline(string_of_int entry.nlocals ^ " -> " ^ name)    
      ) symTable
  ) env
;;

let checkStore (store:store) = 
  if store = IntMap.empty
  then print_endline "Empty.";

  IntMap.iter
  (
    fun key value ->
      print_endline(string_of_int key ^ ": " ^ string_of_int value)
  ) store
;;

let allocateMem (env:environment) : environment =
  let rec loop2 (symTable: symTable): (string*symEntry) list =
    match symTable with
    | [] -> []
    | h::t -> 
        match h with
        | (name, Var entry) -> (
            let newEntry = {entry with loc = !memory} in

            (match newEntry.vtype with
            | Array (_, size) ->
              memory := !memory + (size - 1)
            | _ -> ());
                  
            incr memory;
       	    (name, Var newEntry)::loop2(t))
      	| funEntry ->
            funEntry::loop2(t)
  in

  let rec loop (symTables: environment) =
    match symTables with
    | [] -> []
    | h::t ->
        loop2(h)::loop(t)
  in

  loop(env)
;; (* TODO: implement this function *)

(* eval_expr and eval_cond don't return a store, but you might have to modify
that because in full C-flat they do have side effets (pre- and post-increment,
function calls) *)

(* eval_expr: expr -> proc_state -> env -> store -> int *)
let rec eval_expr (expr:expr) (ps:proc_state) (env:environment) (store:store) : (int*store) = 
  match expr with
    | Add (e1, e2) -> 
        let (r1, store) = eval_expr e1 ps env store in
        let (r2, store) = eval_expr e2 ps env store in
        (r1 + r2, store)
    | Sub (e1, e2) ->
        let (r1, store) = eval_expr e1 ps env store in
      	let (r2, store) = eval_expr e2 ps env store in
        (r1 - r2, store)
    | Mul (e1, e2) ->        
        let (r1, store) = eval_expr e1 ps env store in
        let (r2, store) = eval_expr e2 ps env store in
        (r1 * r2, store)
    | Div (e1, e2) ->
        let (r1, store) = eval_expr e1 ps env store in
        let (r2, store) = eval_expr e2 ps env store in
        (r1 / r2, store)
    | Neg (e1) ->
        let (r1, store) = eval_expr e1 ps env store in
        (0 - r1, store)
    | IntConst i -> 
        (i, store)
    | Id name ->
        let (Var entry) = 
          try
            binding_of env name
          with Not_found ->
            binding_of [List.nth env ((List.length env) - 1)] name
        in
        (value_at store entry.loc, store)
    | Pre (e1) -> (
        let (value, store) = eval_expr e1 ps env store in
        let newValue = value + 1 in
        match e1 with
        | Id name ->
          let (Var entry) = binding_of env name in
          (newValue, (insert_value store entry.loc newValue))
        | Deref ex1 ->
          let (loc, store) = eval_expr ex1 ps env store in
          (newValue, (insert_value store loc newValue))
      )
    | Post (e1) -> (
        let (value, store) = eval_expr e1 ps env store in
        let newValue = value + 1 in
        match e1 with
        | Id name ->
          let (Var entry) = binding_of env name in
          (value, (insert_value store entry.loc newValue))
        | Deref ex1 ->
          let (loc, store) = eval_expr ex1 ps env store in
          (value, (insert_value store loc newValue))
      )
    | At (e1, e2) -> (
      	let (r2, store) = eval_expr e2 ps env store in
      	match e1 with
        | Id name -> (
          let (Var entry) = binding_of env name in
          match entry.vtype with
          | Array (_, size) ->
            if r2 > (size - 1)
            then raise (Invalid_argument "index out of bounds")
            else
          	  (value_at store (entry.loc + r2), store)
          )
        | Deref e1 ->
          let (loc, store) = eval_expr e1 ps env store in
          (value_at store loc, store)
      )
    | Deref (e1) ->
        let (loc, store) = eval_expr e1 ps env store in          
        (value_at store loc, store)
    | AddressOf (e1) -> (
      	match e1 with
      	| Id name ->
          let (Var entry) = 
        	  try
              binding_of env name
            with Not_found ->
              binding_of [List.nth env ((List.length env) - 1)] name
          in
          (entry.loc, store)
        | At (ex1, ex2) -> (
          let (r2, store) = eval_expr ex2 ps env store in
          match ex1 with
          | Id name ->
            let (Var entry) = binding_of env name in
            (entry.loc + r2, store)
          )
      )
    | Call (name, exprs) ->   
        let (Fun entry) = binding_of [List.nth env ((List.length env) - 1)] name in
        let newSyms = List.hd (allocateMem [entry.syms]) in
      	let rec param_loop (params, vars, store) : store =
      		match (params, vars) with
      		| ([], _) -> store
      		| (x::xs, y::ys) -> 
            match y with
      			| (var_name, Var entry) -> 	
      				  let (r1, store) = eval_expr x ps env store in
      				  param_loop(xs, ys, (insert_value store entry.loc r1))
      			| (fun_name, Fun entry) -> 
      				  param_loop(xs, ys, store)
      	in
        let store = param_loop(exprs, newSyms, store) in
        let newEnv = enter_scope env newSyms in
        let (_, ps, store) = eval_stmt (List entry.body) ps newEnv store in

        eval_expr (entry.rv) ps newEnv store

(* TODO: add more *)


(* eval_expr: expr -> proc_state -> env -> store -> bool *)
and eval_cond (cond:cond) (ps:proc_state) (env:environment) (store:store) : (bool*store) = 
  match cond with
    | Equal (e1, e2) ->
        let (r1, store) = eval_expr e1 ps env store in
        let (r2, store) = eval_expr e2 ps env store in
        (r1 == r2, store)
    | Less (e1, e2) ->
        let (r1, store) = eval_expr e1 ps env store in
        let (r2, store) = eval_expr e2 ps env store in
        (r1 < r2, store)
    | And (e1, e2) ->
        let (r1, store) = eval_cond e1 ps env store in
        if not r1
        then (false, store)
        else
          let (r2, store) = eval_cond e2 ps env store in
          (r1 && r2, store)
    | Or (e1, e2) ->
        let (r1, store) = eval_cond e1 ps env store in
        if r1 
        then (true, store)
        else
          let (r2, store) = eval_cond e2 ps env store in
          (r1 || r2, store)
    | Not (e1) ->
        let (r1, store) = eval_cond e1 ps env store in
        (not r1, store)
    | True ->
        (true, store)
    | False ->
        (false, store)

(* TODO: add more *)

(* eval_stmt: stmt -> proc_state -> env -> store -> stmtEvalRes*proc_state*store *)
and eval_stmt (stmt:stmt) (ps:proc_state) (env:environment) (store:store) : (stmtEvalRes*proc_state*store) = 

  match stmt with
  | Empty ->
      (Next, ps, store)

  | Break -> 
      (BreakOut, ps, store)

  | Continue ->
      (ContinueOn, ps, store)

  | Expr (expr) ->      
      let (_, store) = eval_expr expr ps env store in
      (Next, ps, store)

  | VarAss (left, right) -> (
      let (value, store) = eval_expr right ps env store in
      match left with	
      | Id name ->
        let (Var entry) = binding_of env name in
        (Next, ps, (insert_value store entry.loc value))
      | Deref e1 ->
        let (loc, store) = eval_expr e1 ps env store in
        (Next, ps, (insert_value store loc value))
      | At (e1, e2) -> (
        let (index, store) = eval_expr e2 ps env store in
        match e1 with
        | Id name -> (
          let (Var entry) = binding_of env name in
          match entry.vtype with
          | Array (_, size) ->
            if index > (size - 1)
            then raise (Invalid_argument "index out of bounds")
            else
              (Next, ps, (insert_value store (entry.loc + index) value))    
          ) 
        | Deref ex1 -> 
          let (loc, store) = eval_expr ex1 ps env store in
          (Next, ps, (insert_value store (loc + index) value))
        )
    )

  | PrintInt e ->
      let (r, store) = eval_expr e ps env store in
      print_int r; (Next, ps, store)

  | PrintStr s ->
      print_string (Str.global_replace (Str.regexp "\\\\n") "\n" s); 
      (* Escaping characters here because it's not done in the parser *)
      (Next, ps, store)

  | List (stmts) -> (
      match stmts with
      | [] -> (Next, ps, store)
      | h::t ->
        let (next, ps, store) = eval_stmt h ps env store in
        if next = BreakOut || next = ContinueOn
        then (next, ps, store)
        else eval_stmt (List t) ps env store)

  | IfThen (cnd, stm) ->
      let (r1, store) = eval_cond cnd ps env store in
      if r1
    	then 
        eval_stmt stm ps env store
    	else 
        (Next, ps, store);

  | IfThenElse (cnd, stmt1, stmt2) ->
      let (r1, store) = eval_cond cnd ps env store in
      if r1 
    	then 
        eval_stmt stmt1 ps env store
    	else 
        eval_stmt stmt2 ps env store;

  | Switch (cnd, cases, default) ->
      let (r1, store) = eval_expr cnd ps env store in
      let matched = false in	

      let rec loop_through (case_list) =
        match case_list with
        | [] -> eval_stmt (List default) ps env store
        | case::rest ->
          match case with
          | (value, stmts) -> 
            if value = r1
            then 
              eval_stmt (List stmts) ps env store
            else
              loop_through(rest)

      in loop_through(cases);

  | While (cnd, stm) -> 
      	let (r1, store) = eval_cond cnd ps env store in
      	if r1
      	then (	
        	let (next, ps, store) = eval_stmt stm ps env store in
          if next = BreakOut
          then 
            (Next, ps, store)
          else
            eval_stmt (While (cnd, stm)) ps env store)
       	else
          (Next, ps, store);

  | For (stm1, cnd, stm2, stmts) -> 
      let (_, ps, store) = eval_stmt stm1 ps env store in

      let rec inner_loop (ps:proc_state) (store:store) = 
        let (r1, store) = eval_cond cnd ps env store in
        if r1
        then (        
          let (next, ps, store) = eval_stmt stmts ps env store in
          if next = BreakOut
          then 
            (Next, ps, store)
          else
            let (_, ps, store) = eval_stmt stm2 ps env store in
            inner_loop ps store)
        else
          (Next, ps, store) in

      inner_loop ps store;
;;
